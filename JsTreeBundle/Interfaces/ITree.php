<?php

namespace JsTree\JsTreeBundle\Interfaces;

/**
 * Interface ITree
 * @package JsTree\JsTreeBundle\Interfaces
 */
interface ITree
{
    /**
     * Set parent
     *
     * @param ITree $parent
     */
    public function setParent(ITree $parent = null);

    /**
     * Get parent
     *
     * @return ITree
     */
    public function getParent();

    /**
     * Add child
     *
     * @param ITree $child
     */
    public function addChild(ITree $child);

    /**
     * Remove child
     *
     * @param ITree $child
     */
    public function removeChild(ITree $child);

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren();
}
