<?php

namespace JsTree\JsTreeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Class AjaxController
 * @package JsTree\JsTreeBundle\Controller
 */
class AjaxController extends AbstractController
{
}
