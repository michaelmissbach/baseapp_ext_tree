<?php

namespace JsTree\JsTreeBundle\Eventlistener;

use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use JsTree\JsTreeBundle\Entity\Tree;

/**
 * Class EntityExtendListener
 * @package JsTree\JsTreeBundle\Eventlistener
 */
class EntityExtendListener implements Eventsubscriber
{
    /**
     * {@inheritDoc}
     */
    public function getSubscribedEvents()
    {
        return [
            Events::loadClassMetadata,
        ];
    }

    /**
     * @param LoadClassMetadataEventArgs $eventArgs
     * @throws \ReflectionException
     */
    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs)
    {
        // the $metadata is all the mapping info for this class
        $classMetadata  = $eventArgs->getClassMetadata();
        $reflObj = new \ReflectionClass($classMetadata->name);
        if($reflObj) {
            if ($reflObj->isSubclassOf(Tree::class)) {
                $fieldMapping = array(
                    'targetEntity'  => $classMetadata->name,
                    'fieldName'     => 'parent',
                    'inversedBy'    => 'children',
                    'JoinColumn'    => array(
                        'name'                  => 'parent_id',
                        'referencedColumnName'  => 'id',
                        'nullable'              => true,
                        'onDelete'              => 'SET NULL',
                    ),
                );

                $classMetadata->mapManyToOne($fieldMapping);


                $fieldMapping = array(
                    'fieldName'     => 'children',
                    'targetEntity'  => $classMetadata->name,
                    'mappedBy'      => 'parent',
                );
                $classMetadata->mapOneToMany($fieldMapping);
            }
        }
    }
}
