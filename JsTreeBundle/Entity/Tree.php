<?php

namespace JsTree\JsTreeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsTree\JsTreeBundle\Interfaces\ITree;

/**
 * Class Tree
 * @package JsTree\JsTreeBundle\Entity
 *
 * @ORM\MappedSuperclass
 */
class Tree implements ITree
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var ITree
     */
    protected $parent;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    protected $children;

    /**
     * Set parent
     *
     * @param ITree $parent
     */
    public function setParent(ITree $parent = null)
    {
        $this->parent = $parent;
    }

    /**
     * Get parent
     *
     * @return ITree
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add child
     *
     * @param ITree $child
     */
    public function addChild(ITree $child)
    {
        $this->children[] = $child;
    }

    /**
     * Remove child
     *
     * @param ITree $child
     */
    public function removeChild(ITree $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }
}
