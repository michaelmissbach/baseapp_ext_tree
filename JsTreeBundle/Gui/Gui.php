<?php

namespace JsTree\JsTreeBundle\Gui;

use BaseApp\BaseappBundle\Builder\Gui\GuiBuilder;
use BaseApp\BaseappBundle\Builder\Gui\Interfaces\IGui;
use BaseApp\BaseappBundle\Builder\Gui\Elements\Incs\InternJavascript;
use BaseApp\BaseappBundle\Builder\Gui\Elements\Incs\InternStyleSheet;
use BaseApp\BaseappBundle\Builder\Gui\Elements\Sidebar\Level1Divider;
use BaseApp\BaseappBundle\Builder\Gui\Elements\Sidebar\Level1MenuEntry;
use BaseApp\BaseappBundle\Builder\Gui\Elements\Sidebar\Level1MenuHeadline;
use BaseApp\BaseappBundle\Builder\Gui\Elements\Sidebar\Level1MenuEntryWithChilds;

/**
 * Undocumented class
 */
class Gui implements IGui
{/**
     * @param GuiBuilder $guiBuilder
     */
    public function create(GuiBuilder $guiBuilder): void
    {        
        $headStyle = $guiBuilder->getHeadStyleSheets();
        $headStyle->add(InternStyleSheet::create()->setTitle('/bundles/cms/css/themes/default/style.min.css'),10);

        $headJs = $guiBuilder->getHeadJavascripts();
        $headJs->add(InternJavascript::create()->setTitle('/bundles/cms/js/jstree.min.js'));
        $headJs->add(InternJavascript::create()->setTitle('/bundles/cms/js/baseapp_tree.js'),10);

        $sidebar = $guiBuilder->getSidebar();
        $sidebar->add(Level1Divider::create());
        $sidebar->add(Level1MenuHeadline::create()->setTitle('Cms'));
        $sidebar->add(
            Level1MenuEntry::create()
                ->setTitle('Pages')->setIcon('fas fa-fw fa-tachometer-alt')/*->setRouteName('cms_page')*/
        );
        /*$container = $guiBuilder->createGuiContainer();
        $sidebar->add(
            Level1MenuEntryWithChilds::create()
                ->setChildContainer($container)
                ->setTitle('Config')->setIcon('fas fa-fw fa-table')
        );*/
    }

    /**
     * @param GuiBuilder $guiBuilder
     */
    public function modify(GuiBuilder $guiBuilder): void
    {
    }
}
